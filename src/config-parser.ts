import * as os from 'os';
import { promises as fs } from 'fs';

export interface Configuration {
    templateDirs: string[]
}

export class ConfigParser {
    public async  readConfiguration(): Promise<Configuration> {
        const configurationFilePath = os.homedir() + '/yamac_config.json'
        const data = await fs.readFile(configurationFilePath);
        return JSON.parse(data.toString());
    }
}
