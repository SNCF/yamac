import Handlebars from 'handlebars';
import fs from 'fs';
import { Configuration } from './config-parser';

export class TemplateRenderer {
    public render(templateName: string, content: string, properties: any, configuration: Configuration): string {
        let templateContent: string | undefined = undefined;
        for (const templateDir of configuration.templateDirs) {
            const templatePath = templateDir + '/' + templateName + ".html";
            if (fs.existsSync(templatePath)) {
                templateContent = fs.readFileSync(templatePath).toString()
            }
        }
        if (!templateContent) throw new Error('Cannot find template ' + templateName + ' in templateDirs.');
        const hdl = Handlebars.compile(templateContent);
        return hdl({content, ...properties}).toString();
    }
}
