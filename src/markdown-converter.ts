import fs from 'fs';
import MarkdownIt from 'markdown-it';
import yaml = require('js-yaml');

export class MarkdownConverter {
    public convert(pathToFileToConvert: string): ConversionResult {
        let markdownProperties: any;
        const markdownIt = new MarkdownIt({
            html: true,
            linkify: true,
            quotes: ['«\xA0', '\xA0»', '‹\xA0', '\xA0›'],
            typographer: true
        })
            .use(require('markdown-it-highlightjs'))
            .use(require('markdown-it-table-of-contents'), {format: (content: any, md: any, link: any) => '<span class="toc-title">' + md.renderInline(content) + '</span><span class="toc-page" data-url="' + link + '"></span>'})
            .use(require('markdown-it-anchor'))
            .use(require('markdown-it-attrs'))
            .use(require('markdown-it-front-matter'), (mdFrontMatterData: any) => {
                markdownProperties = yaml.load(mdFrontMatterData);

            });
        const data = fs.readFileSync(pathToFileToConvert);
        const userContent = markdownIt.render(data.toString());
        return {
            content: userContent,
            properties: markdownProperties
        }
    }
}

export interface ConversionResult {
    content: string;
    properties: any;
}
