import { execSync } from "child_process";


export class PagedJsRunner {
    public run(filepath: string, outputName: string): void {
        console.log("Start pagedjs conversion");
        const pagedJsProcessOutput = execSync(`pagedjs-cli ${filepath} -o ${outputName}.pdf`, {encoding: 'utf-8'});
        console.log(pagedJsProcessOutput);
    }
}
