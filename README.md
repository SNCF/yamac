# YAMAC

Yet Another Markdown to PDF CLI converter

## Goals

- Provide a tool to generate PDF documents from a template shared between different teams while letting the user add
  local styles to his document
- Allow presentation generation (without animations)
- Provide the necessary features to edit documents "out of the box" (generation of table of contents, footnotes, etc.)

## Features

- markdown to pdf conversion
- css for styling
- global templates that can be located in different folders
- Syntax highlighting with highlight.js (don't forget to add highlight.js theme in your stylesheet if you want to use
  it!)
- Table of content automatic generation from your headings (with : https://github.com/cmaas/markdown-it-table-of-contents)
- Anchors
- Templating with [mustache](https://mustache.github.io/)
- Markdown front matter to initialize js variable for your template
- Markdown attr (with : https://github.com/arve0/markdown-it-attrs)

## Non Goals

- Provide a multi-format conversion tool, if you want a magic stuff, use pandoc 😉

## Documentation

### Setup

- First you have to add pagedjs-cli (globally) to your system : `npm install -g pagedjs-cli`
- Then, depending on your system, use the appropriate binary by adding it to your path

### User configuration file

To configure Yamac, **you must** create a configuration file in Json format named "yamac_config.json" in your home
directory.

The following configuration keys are available:

| key          | required    | type  | description                                                                                                 |
| ------------ | ----------- | ----- | ----------------------------------------------------------------------------------------------------------- |
| templateDirs | yes         | array | Contains the list of folders in which yamac will search for templates for document generation               |

### Creating templates

The template argument allows you to select the template to use to generate the document. Yamac will then look for a
file named {{template_name}}.html in all directories present in the "templateDirs" configuration key.

Templates are in html format and their context path when generated will be the one from which yamac is called, so avoid
using relative paths in templates.

For template interpretation, handlebars is used. A variable named {{content}} will be injected automatically with the
html content of the markdown file converted by the user.

The user can also provide other JS objects in his markdown file thanks to the front matter in YAML format. All these
yaml properties will be injected as JS objects in handlebars

### Example

1. Create the `yamac_config.json` in your home directory with the following content :

```json
{
  "templateDirs": [
    "C:\\Users\\{username}\\yamac-templates"
  ]
}
```

2. Create the following html template `demo.html` in `C:\Users\{username}\yamac-templates`

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="C:\Users\{username}\yamac-templates\demo.css">
    <meta charset="UTF-8">
    <title>{{title}} - {{subtitle}}</title>
</head>
<body>
<div class="footer-service">
    <span style="font-weight: bold;">My Company Name</span>
</div>
<img class="footer-logo" src="https://www.sncf.com/themes/contrib/sncf_theme/images/logo-sncf.svg"/>
<div class="cover">
    <div class="cover-content">
        <div class="cover-title">{{title}}</div>
        <div class="cover-subtitle">{{subtitle}}</div>
        <img class="cover-logo" src="https://www.sncf.com/themes/contrib/sncf_theme/images/logo-sncf.svg"/>
    </div>
</div>
{{{content}}}
</body>
</html>
```

3. Create the following css template `demo.css` in `C:\Users\{username}\yamac-templates`

```css
html, body {
    font-family: sans-serif;
}

@page :first {
    @bottom-center {
        content: none;
    }
    @bottom-left {
        content: none;
    }
    @bottom-right {
        content: none;
    }
}

@page {
    size: A4;
    margin: 1cm 1.5cm 2cm 1.5cm;
    @bottom-left {
        content: element(footerService);
        padding-bottom: 1cm;
    }
    @bottom-center {
        content: "Page " counter(page) " sur " counter(pages);
        font-size: 10pt;
        padding-bottom: 1cm;
    }
    @bottom-right {
        content: element(footerLogo);
        padding-bottom: 1cm;
    }
}

.first-page-footer {
    position: running(firstPageFooter);
    font-size: 10pt;
}

.footer-service {
    position: running(footerService);
    font-size: 10pt;
}

.footer-logo {
    position: running(footerLogo);
    height: 0.8cm;
    float: right;
}

h1 {
    border-bottom: solid 2px black;
}

p {
    text-align: justify;
}

.first-page-footer {
    position: running(firstPageFooter);
    font-size: 10pt;
}

.footer-service {
    position: running(footerService);
    font-size: 10pt;
}

.footer-logo {
    position: running(footerLogo);
    height: 0.8cm;
    float: right;
}

blockquote {
    border-left: solid 4px dodgerblue;
    background-color: beige;
    padding: 1px 0.5cm;
}

/********************
 *      COVER       *
 ********************/

.cover {
    page-break-after: always;
}

.cover-content {
    margin-top: 8cm;
}

.cover-title {
    font-size: 42px;
    text-transform: uppercase;
    font-weight: 700;
    color: dodgerblue;
}

.cover-subtitle {
    font-size: 30px;
    color: dodgerblue;
}

.cover-logo {
    width: 2cm;
    margin-top: 1cm;
}

.cover-footer {
    text-align: center;
    position: absolute;
    bottom: 1cm;
    width: 100%;
}
```

4. create a markdown file with the following content

```md
---
title: Test de titre
subtitle: Sous titre de test
---

# Ceci est un titre de niveau 1

Logoden biniou degemer mat an penn ar bed evidomp, truez davañjer bodet holl a gwener diwezhañ endervezh gorre, nadoz
c’har sankañ sukr vered ugnet marv. Kotoñs Brasparz kroaz klevout c’hwi klouar c’hoef egisti sul, sukr chase stumm
c’hwevrer dek tud penn hent poent, kouezhañ kalonek lun pennad ar eme eost. Leal marv Lokmikael (an-Traezh) disul
fentigelloù kuzuliañ kerzu gwalenn heñvel, koll c’horn vrec’h c’hewz embann loar.

## Titre de niveau 2

Klevout bier eost neñv vuoc’h oan mae netra droug, ebeul hepken troc’hañ diwall evit kartoñs kouezhañ biken dreist, dant
glin war Pont-Aven egisti kaol Malo. Dir sukr fall mezheven huchal kozh an kreisteiz dilhad, disadorn roud sizailh ur
fri riskl dit eta Aradon, seul.

> An anezhañ lazhañ skuizhañ gwechall buhez kotoñs naontek niver, Montroulez amprevan ken santout tachenn c’hafe skrabañ
Planvour kaer, ur eo bodet regiñ te sac’h gwin. Ler ganeoc'h falc’hat ouzhoc'h skevent gouez yar kardeur c'hanon, alies
enep Argol druez kazetenn c’havr gorre gaoued chadenn.

Leskiñ beuziñ bez Entraven soñjal ar skolaer ezhomm naetaat, eoul ar ael voulouz kousket ifern stivell daoust ganit,
feunten gwinegr divskouarn egistoc'h marennañ tenn skouer. Oferenn votez diskar anal evezh mar porzh e esaeañ, Briad
kelc’h gouiziek ar pri kas hiziv triwec’h broust, prest hanternoz mesk bann prenañ kennebeut enebour.

Rodell paotr laerezh maouez drezon respont Pederneg fri daoust, digor c’hwevrer kennebeut miz gentañ divalav tra c’hein
Sant-Maloù, garmiñ pluenn gozh outo dek etrezek kargañ. Bed ha Mederieg skeudenn a plijet gwastell lost gegin, serret
Sant-Gwenole an mintin kerc’h Pont’n-Abad teñvalijenn wouel lezel, muioc’h da wenodenn Roazhon Park kezeg ar Skos.
Degemer lezirek Gerveur nizez.
```

5. Convert your file in pdf with yamac `$ yamac -t demo you_file.md`

## Build

To create an executable, you have to [install](https://github.com/yao-pkg/pkg) `@yao-pkg/pkg` globally and run the following command :

```shell
npm run build:bin
```

> We try to use the new node sea feature, but the tool isn't mature enough to be used in production. So we decided to use the forked version of pkg.

## Ressources 

### base stylesheet for TOC

```css
/********************
 *       TOC        *
 ********************/

.table-of-contents {
    overflow-x: hidden;
    page-break-after: always;
}

.table-of-contents a {
    padding: 0;
    text-decoration: none;
    color: black;
}

.table-of-contents .toc-title {
    background: white;
}

.table-of-contents .toc-title::after {
    content: '.............................' '......................................' '......................................' '......................................' '......................................';
    float: left;
    width: 0;
    letter-spacing: 2px;
    color: #999;
}

.table-of-contents .toc-page {
    background: white;
    padding-left: 0.33em;
    float: right;
}

.table-of-contents .toc-page::after {
    content: target-counter(attr(data-url url), page);
}
```